# System required imports
import sys
import glob
from time import sleep
import binascii
import threading

# Method required imports
import serial
import serial.tools.list_ports

ser = None
read_thread = None
connected = False

updated = False
latestcontent = None

pool = dict()

def cmd_lookup(x):
    return {
        0x01 : "CMD_HELLO", 
        0x02 : "CMD_HEART_BEAT", 
        0x03 : "CMD_GET_MODULE_INFO", 
        0x22 : "CMD_SINGLE_ID", 
        0x27 : "CMD_MULTI_ID", 
        0x28 : "CMD_STOP_MULTI", 
        0x39 : "CMD_READ_DATA", 
        0x49 : "CMD_WRITE_DATA", 
        0x82 : "CMD_LOCK_UNLOCK", 
        0x65 : "CMD_KILL", 
        0x07 : "CMD_SET_REGION", 
        0xA9 : "CMD_INSERT_FHSS_CHANNEL", 
        0xAA : "CMD_GET_RF_CHANNEL", 
        0xAB : "CMD_SET_RF_CHANNEL", 
        0xAF : "CMD_SET_CHN2_CHANNEL", 
        0xAC : "CMD_SET_US_CHANNEL",  #For RFCONN Conference
        0xAE : "CMD_OPEN_PA",  #For RFCONN Conference
        0xAD : "CMD_SET_FHSS", 
        0xB6 : "CMD_SET_POWER", 
        0xB7 : "CMD_GET_POWER", 
        0x0B : "CMD_GET_SELECT_PARA", 
        0x0C : "CMD_SET_SELECT_PARA", 
        0x0D : "CMD_GET_QUERY_PARA", 
        0x0E : "CMD_SET_QUERY_PARA", 
        0xB0 : "CMD_SET_CW", 
        0xBF : "CMD_SET_BLF", 
        0xFF : "CMD_FAIL", 
        0x00 : "CMD_SUCCESS", 
        0xFE : "CMD_SET_SFR", 
        0xFD : "CMD_READ_SFR", 
        0xEC : "CMD_INIT_SFR", 
        0xEA : "CMD_CAL_MX", 
        0xED : "CMD_CAL_LPF", 
        0xFB : "CMD_READ_MEM", 
        0x12 : "CMD_SET_INV_MODE", 
        0x11 : "CMD_SET_UART_BAUDRATE", 
        0xF2 : "CMD_SCAN_JAMMER", 
        0xF3 : "CMD_SCAN_RSSI", 
        0xF4 : "CMD_AUTO_ADJUST_CH", 
        0xF0 : "CMD_SET_MODEM_PARA", 
        0xF1 : "CMD_READ_MODEM_PARA", 
        0xF5 : "CMD_SET_ENV_MODE", 
        0x55 : "CMD_TEST_RESET", 
        0x17 : "CMD_POWERDOWN_MODE", 
        0x1D : "CMD_SET_SLEEP_TIME", 
        0x1A : "CMD_IO_CONTROL", 
        0x19 : "CMD_RESTART", 
        0x0A : "CMD_LOAD_NV_CONFIG", 
        0x09 : "CMD_SAVE_NV_CONFIG", 
        0x1F : "CMD_ENABLE_FW_ISP_UPDATE", 
        0x14 : "CMD_SET_READ_ADDR", 
    }[x]

class cmd:
    CMD_HELLO = 0x01
    CMD_HEART_BEAT = 0x02
    CMD_GET_MODULE_INFO = 0x03
    CMD_SINGLE_ID = 0x22
    CMD_MULTI_ID = 0x27
    CMD_STOP_MULTI = 0x28
    CMD_READ_DATA = 0x39
    CMD_WRITE_DATA = 0x49
    CMD_LOCK_UNLOCK = 0x82
    CMD_KILL = 0x65
    CMD_SET_REGION = 0x07
    CMD_INSERT_FHSS_CHANNEL = 0xA9
    CMD_GET_RF_CHANNEL = 0xAA
    CMD_SET_RF_CHANNEL = 0xAB,
    CMD_SET_CHN2_CHANNEL= 0xAF
    CMD_SET_US_CHANNEL = 0xAC #For RFCONN Conference
    CMD_OPEN_PA = 0xAE #For RFCONN Conference
    CMD_SET_FHSS = 0xAD
    CMD_SET_POWER = 0xB6
    CMD_GET_POWER = 0xB7
    CMD_GET_SELECT_PARA = 0x0B
    CMD_SET_SELECT_PARA = 0x0C
    CMD_GET_QUERY_PARA = 0x0D
    CMD_SET_QUERY_PARA = 0x0E
    CMD_SET_CW = 0xB0
    CMD_SET_BLF = 0xBF
    CMD_FAIL = 0xFF
    CMD_SUCCESS = 0x00
    CMD_SET_SFR = 0xFE
    CMD_READ_SFR = 0xFD
    CMD_INIT_SFR = 0xEC
    CMD_CAL_MX = 0xEA
    CMD_CAL_LPF = 0xED
    CMD_READ_MEM = 0xFB
    CMD_SET_INV_MODE = 0x12
    CMD_SET_UART_BAUDRATE = 0x11
    CMD_SCAN_JAMMER = 0xF2
    CMD_SCAN_RSSI = 0xF3
    CMD_AUTO_ADJUST_CH = 0xF4
    CMD_SET_MODEM_PARA = 0xF0
    CMD_READ_MODEM_PARA = 0xF1
    CMD_SET_ENV_MODE = 0xF5
    CMD_TEST_RESET = 0x55
    CMD_POWERDOWN_MODE = 0x17
    CMD_SET_SLEEP_TIME = 0x1D
    CMD_IO_CONTROL = 0x1A
    CMD_RESTART = 0x19
    CMD_LOAD_NV_CONFIG = 0x0A
    CMD_SAVE_NV_CONFIG = 0x09
    CMD_ENABLE_FW_ISP_UPDATE = 0x1F
    CMD_SET_READ_ADDR = 0x14


class MemoryBank:
    Reserved = 0x00
    EPC = 0x01
    TID = 0x02
    User = 0x03


def error_code(x):
    return {
        0x17 : "CMD error",
        0x20 : "Search Channel Failed",
        0x15 : "Invertory Query error",
        0x16 : "Invertory Access Password error",
        0x09 : "Memory Read failed",
        0xA0 : "Memory Read error",
        0x10 : "Memory Write failed",
        0xB0 : "Memory Write error",
        0x13 : "Memory Loack Failed",
        0x12 : "Invertory Kill Failed",
        0xD0 : "Invertory Kill Error",
        0x2A : "ReadProtect Failed",
        0x2B : "Reset ReadProtect Failed",
        0x1b : "Change EAS Failed",
        0xE0 : "NXP Error",
        0xA4 : "Memory Overrun"
    }[x]

class HardwareError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def getEnvPorts():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

def read_from_port(ser):
    global connected
    while not connected:
        connected = True

        while True:
            line = ser.read_until("\x8e")
            if len(line)>0:
                print("Data Received")
                handle_data(line)
            sleep(0.01)
            
        #    reading = ser.readline().decode()
        #    handle_data(reading)

def openPort(port):
    """PLEASE DOCUMENT THIS FUCK"""
    global ser
    global read_thread
    ser = serial.Serial(port, 115200, timeout=1)# open serial port '/dev/ttyUSB0' 'COMxx'
    
    read_thread = threading.Thread(target=read_from_port, args=(ser,))
    read_thread.start()
    return read_thread.is_alive

def closePort():
    """PLEASE DOCUMENT THIS FUCK"""
    if ser is not None:
        ser.close()
        return not ser.is_open
    else:
        raise HardwareError("The device is not opened yet")





# def cmd_readInventory():
#     if ser is not None:
#         ser.write(serial.to_bytes([0xaa,0x00,0x22,0x00,0x00,0x22,0x8e])) 
#         line = ser.readline()
#         packet = unpack(line)
#         print (packet)
#         return line.hex()
#     else:
#         raise HardwareError("The device is not opened yet")
        
# def cmd_readLoopInventory(times):
#     if ser is not None:
#         times.to_bytes(2, byteorder='big')
#         write_packet=[0xaa,0x00,0x27,0x00,0x03,0x22]
#         write_packet.extend(times.to_bytes(2, byteorder='big')) #add number of time
#         write_packet.extend(checksum(write_packet)) #add check checksum
#         write_packet.extend(0x8e) #add end code
#         ser.write(write_packet) 
#         line = ser.readline()
#         packet = unpack(line)
#         print (packet)
#         return line.hex()
#     else:
#         raise HardwareError("The device is not opened yet")

def checksum(obj):
    if len(hex(sum(obj)))>3:
        return int(hex(sum(obj))[-2:],16)
    else:
        return int(hex(sum(obj))[-1:],16)

def crc16(data):
    regCRC = 0xffff
    generator = 0x1021
    for i in data:
        regCRC = regCRC ^ (i<<8)
        for j in range(0, 8):
            if (regCRC & 0x8000) !=0 :
                regCRC = ((regCRC) << 1)& 0xffff ^ generator
            else:
                regCRC = regCRC << 1
    return regCRC ^ 0xffff

def addpool(data):
    if data['EPC'] in pool:
        pool[data['EPC']]['count'] += 1
    else:
        pool[data['EPC']] = data.copy()
        pool[data['EPC']]['count'] = 1


def PL_extract(cmd,PL):
    print("Payload: " + PL.hex())
    payload = dict()

    print(cmd_lookup(cmd))

    if cmd==0x03:
        print("Info ack")
    elif cmd==0x22:
        print("invertory return")
        RSSI=PL[0]
        PC=PL[1]*256+PL[2]
        EPC=PL[3:-2]
        CRC=PL[-2]*256+PL[-1]
        PCnEPC = PL[1:-2]
        Computed_CRC=crc16(PCnEPC)
        payload={
            'RSSI' : RSSI,
            'PC'   : hex(PC),
            'EPC'  : EPC.hex(),
            # 'PCnEPC': PCnEPC.hex(),
            # 'CRC'  : hex(CRC),
            'Verify' : hex(Computed_CRC)==hex(CRC)
        }
        addpool(payload)
        return payload
    elif cmd==0xFF:
        print("Error Handling - Command error")
        print(error_code(PL[0]))
    else:
        print("Error Handling - Unimplememted error")
        print(str(PL[0]))

def index(a,value):
    try:
        index_value = a.index(value)
    except ValueError:
        index_value = -1
    return index_value

def handle_packet(packet):
    packet_content = dict()

    raw = packet

    if raw[0] != 0xaa or raw[-1] != 0x8e:
        return None

    packet_type = raw[1]

    if packet_type >0x00:
        packet_cmd = raw[2]
        packet_pl_len = int(raw[3])*256+int(raw[4])
        packet_checksum = raw[-2]
        computed_checksum = checksum(raw[1:-2])
        
        if computed_checksum == packet_checksum :
            print("checksum correct")
            packet_content.update({'checksum_correct' : True})
            if packet_pl_len>0:
                packet_content.update({'contents' : PL_extract(packet_cmd,raw[5:5+packet_pl_len])})
        else:
            print("checksum failed")
            packet_content.update({ 'checksum_correct' : False,
                                    'computed_checksum': hex(computed_checksum),
                                    'packet_checksum'  : hex(packet_checksum)})
        global latestcontent
        global updated
        latestcontent = packet_content
        updated = True
        return True

    print("No CMD sould be return from Reader")
    return False
    ##Raise error

def handle_data(rawline):
    raw = bytes(rawline)
    print("Raw packet: " + rawline.hex())

    packetlist=[]

    readcur=0
    startcur = -1
    endcur = -1

    while readcur<len(raw):
        startcur = index(raw[readcur:],0xAA)
        endcur = index(raw[readcur:],0x8E)+1

        if startcur !=-1 and endcur !=-1:
            #saveit and move to next readcur
            packetlist.append(list(raw[readcur+startcur:readcur+endcur]))
            readcur += endcur
        else:
            break
            
    for packet in packetlist:
        handle_packet(bytes(packet))


    
def waitforreplytimeout():
    global updated
    global latestcontent
    for i in range(1, 20):
        sleep(0.1)
        if updated==True:
            returnpayload = latestcontent.copy()
            latestcontent = None
            updated = None
            return returnpayload

def clearlatestcontent():
    global updated
    global latestcontent
    latestcontent = None
    updated = None

def newcmd(option):
    return [0xAA,0x00,option]

def attachPL(PL):
    if PL is None or len(PL) ==0:
        return [0x00,0x00]
    else:
        length = len(PL)
        b = length.to_bytes(2,"big")
        res = []
        res.extend([b[0],b[1]])
        res.extend(PL)
        print(res)
        return res

def cmdHello():
    if ser is not None:
        clearlatestcontent()
        mycmd = newcmd(cmd.CMD_HELLO)
        mycmd.extend([checksum(bytes(mycmd[1:]))])
        mycmd.extend([0x8E])
        print(bytes(mycmd).hex())
        ser.write(serial.to_bytes(mycmd)) 

        return waitforreplytimeout()

    else:
        raise HardwareError("No Hardware")

def readTag():
    if ser is not None:
        clearlatestcontent()
        mycmd = newcmd(cmd.CMD_SINGLE_ID)
        mycmd.extend(attachPL(None))
        mycmd.extend([checksum(bytes(mycmd[1:]))])
        mycmd.extend([0x8E])
        print(bytes(mycmd).hex())
        ser.write(serial.to_bytes(mycmd)) 

        return waitforreplytimeout()
    else:
        raise HardwareError("No Hardware")

def readTagData():
    if ser is not None:
        clearlatestcontent()

        #Create Cmd Payload
        PL = []
        password = [0x00,0x00,0x00,0x00]
        PL.extend(password)#attach Access password
        PL.extend([MemoryBank.TID])#select read for User space
        PL.extend([0x00,0x00])# Start Address
        PL.extend([0x00,0x04])# Data Length

        #Create Cmd and Attach Payload
        mycmd = newcmd(cmd.CMD_READ_DATA)
        mycmd.extend(attachPL(PL))
        mycmd.extend([checksum(bytes(mycmd[1:]))])
        mycmd.extend([0x8E])

        print(bytes(mycmd).hex())

        ser.write(serial.to_bytes(mycmd)) 

        return waitforreplytimeout()
    else:
        raise HardwareError("No Hardware")

def readTags():
    if ser is not None:

        clearlatestcontent()

        #Create Cmd Payload
        CNT = 7
        PL = []
        PL.extend([0x22])#Reserved
        PL.extend(list(CNT.to_bytes(2,"big")))# Number of CNT

        #Create Cmd and Attach Payload
        mycmd = newcmd(cmd.CMD_MULTI_ID)
        mycmd.extend(attachPL(PL))
        mycmd.extend([checksum(bytes(mycmd[1:]))])
        mycmd.extend([0x8E])

        print(bytes(mycmd).hex())
        ser.write(serial.to_bytes(mycmd)) 

        return waitforreplytimeout()
    else:
        raise HardwareError("Hardware Error")

def stopGet():
    if ser is not None:
        #Create Cmd and Attach Payload
        mycmd = newcmd(cmd.stopGet)
        mycmd.extend(attachPL(None))
        mycmd.extend([checksum(bytes(mycmd[1:]))])
        mycmd.extend([0x8E])

        print(bytes(mycmd).hex())
        ser.write(serial.to_bytes(mycmd)) 

        return waitforreplytimeout()
    else:
        raise HardwareError("Hardware Error")


def writeTag():
    if ser is not None:
        return True
    else:
        raise HardwareError("Hardware Error")
