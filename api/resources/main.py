"""Entrypoint of the main API Resources."""
# Flask based imports
from flask_restplus import Resource, Namespace, fields,marshal,reqparse,abort
from flask import request
import json

# method required imports
import reader.hardware as hw

# Empty name is required to have the desired url path
api = Namespace(name='', description='Main API namespace.')
parser = reqparse.RequestParser()

@api.route('/hello/<name>')
@api.doc(params={'name': 'The name of the person to return hello.'})
class HelloWorld(Resource):
    """HelloWorld resource class."""

    def get(self, name):
        """Get method."""
        return {'hello': name}

@api.route('/listPort')
class listPort(Resource):
    def get(self):
        """PLEASE DOCUMENT THIS FUCK"""
        list_ports = hw.getEnvPorts()
        return {
            'list_ports': list_ports
            },200



initDevice_Field = api.model('Test', {
  'port': fields.String(required=True, description='The hardware TTY/COM device path', example='COM1')
})

@api.route('/initDevice')
class initDevice(Resource):
    @api.expect(initDevice_Field, validate=True)
    def post(self):
        """PLEASE DOCUMENT THIS FUCK"""
        port = request.json['port']
        try:
            return_payload = hw.openPort(port)
            return {
                'method' : __class__.__name__,
                'params' : request.json,
                'message': "device created",
            }, 201
        except hw.HardwareError as e:
            return {
                'method'  : __class__.__name__,
                'message' : e.value
            },500



@api.route('/closeDevice')
class closeDevice(Resource):
    def get(self):
        """PLEASE DOCUMENT THIS FUCK"""

        try:
            return_payload = hw.closePort()
            return {
                'method': __class__.__name__,
                'message': return_payload,
            }, 202
        except hw.HardwareError as e:
            return {
                'method'  : __class__.__name__,
                'message' : e.value
            },500

@api.route('/readTag')
class readTag(Resource):
    # @api.expect(initDevice_Field, validate=True)
    def get(self):
        """PLEASE DOCUMENT THIS FUCK"""
        try:
            payload = hw.readTag()
            returndata = dict()
            returndata.update({'method': __class__.__name__})
            if payload is not None:
                returndata.update({'data':payload})
                return returndata,200
            else:
                returndata.update({'message':"device reply timeout"})
                return returndata,408
        except hw.HardwareError as e:
            return {
                'method'  : __class__.__name__,
                'message' : e.value
                },500

@api.route('/readTags')
class readTags(Resource):
    # @api.expect(initDevice_Field, validate=True)
    def get(self):
        """PLEASE DOCUMENT THIS FUCK"""
        try:
            payload = hw.readTags()
            returndata = dict()
            returndata.update({'method': __class__.__name__})
            if payload is not None:
                returndata.update({'data':payload})
                return returndata,200
            else:
                returndata.update({'message':"device reply timeout"})
                return returndata,408
        except hw.HardwareError as e:
            return {
                'method'  : __class__.__name__,
                'message' : e.value
                },500

@api.route('/getPool')
class getPool(Resource):
    # @api.expect(initDevice_Field, validate=True)
    def get(self):
        """PLEASE DOCUMENT THIS FUCK"""
        try:
            payload = hw.pool
            returndata = dict()
            returndata.update({'method': __class__.__name__})
            print(payload)
            if len(payload) == 0:
                return returndata,201
            returndata.update({'data':payload})
            return returndata,200
        except hw.HardwareError as e:
            return {
                'method'  : __class__.__name__,
                'message' : e.value
                },500


@api.route('/clrPool')
class clrPool(Resource):
    # @api.expect(initDevice_Field, validate=True)
    def get(self):
        """PLEASE DOCUMENT THIS FUCK"""
        try:
            hw.pool.clear()
            returndata = dict()
            returndata.update({'method': __class__.__name__})
            return returndata,200
        except hw.HardwareError as e:
            return {
                'method'  : __class__.__name__,
                'message' : e.value
                },500

@api.route('/readTagData')
class readTagData(Resource):
    # @api.expect(initDevice_Field, validate=True)
    def get(self):
        """PLEASE DOCUMENT THIS FUCK"""
        try:
            payload = hw.readTagData()
            returndata = dict()
            returndata.update({'method': __class__.__name__})
            if payload is not None:
                returndata.update({'data':payload})
                return returndata,200
            else:
                returndata.update({'message':"device reply timeout"})
                return returndata,408
        except hw.HardwareError as e:
            return {
                'method'  : __class__.__name__,
                'message' : e.value
                },500

@api.route('/stopGet')
class stopGet(Resource):
    # @api.expect(initDevice_Field, validate=True)
    def get(self):
        """PLEASE DOCUMENT THIS FUCK"""
        try:
            payload = hw.stopGet()
            returndata = dict()
            returndata.update({'method': __class__.__name__})
            if payload is not None:
                returndata.update({'data':payload})
                return returndata,200
            else:
                returndata.update({'message':"device reply timeout"})
                return returndata,408
        except hw.HardwareError as e:
            return {
                'method'  : __class__.__name__,
                'message' : e.value
                },500

@api.route('/cmdHello')
class cmdHello(Resource):
    # @api.expect(initDevice_Field, validate=True)
    def get(self):
        """PLEASE DOCUMENT THIS FUCK"""
        try:
            payload = hw.cmdHello()
            returndata = dict()
            returndata.update({'method': __class__.__name__})
            print(payload)
            if len(payload) == 0:
                return returndata,201
            returndata.update({'data':payload})
            return returndata,200
        except hw.HardwareError as e:
            return {
                'method'  : __class__.__name__,
                'message' : e.value
                },500